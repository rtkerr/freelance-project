<?php

class Dashboard extends Controller
{

    // magic constructor to perform magic construction
    public function __construct()
    {
        $this->title = 'Dashboard';
    }

    // default function
    public function index()
    {
        $this->Gate = $this->path('dashboard');
    }
    
}
