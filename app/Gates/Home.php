<?php

class Home extends Controller
{

    // magic constructor to perform magic construction
    public function __construct()
    {
        $this->title = 'Freelance Project';
    }

    // default function
    public function index()
    {
        $this->Gate = $this->path('home');
    }
    
}
