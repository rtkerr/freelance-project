<?php

class Clients extends Controller
{

    // magic constructor to perform magic construction
    public function __construct()
    {
        $this->title = 'Clients';
    }

    // default function
    public function index()
    {
        $this->Gate = $this->path('clients');
    }

}
