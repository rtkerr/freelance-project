<?php

class Projects extends Controller
{

    // magic constructor to perform magic construction
    public function __construct()
    {
        $this->title = 'Projects';
    }

    // default function
    public function index()
    {
        $this->Gate = $this->path('projects');
    }
    
}
