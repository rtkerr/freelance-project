<?php

class Invoices extends Controller
{

    // magic constructor to perform magic construction
    public function __construct()
    {
        $this->title = 'Invoices';
    }

    // default function
    public function index()
    {
        $this->Gate = $this->path('invoices');
    }

}
