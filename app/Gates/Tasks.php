<?php

class Tasks extends Controller
{


    // magic constructor to perform magic construction
    public function __construct()
    {
        $this->title = 'Tasks';
    }

    // default function
    public function index()
    {
        $this->Gate = $this->path('tasks');
    }
    
}
