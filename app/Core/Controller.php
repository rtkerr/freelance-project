<?php
/*
Default controller document
 */

// Class start
class Controller
{

    // default htm document title
    public $title = 'Base';

    // database connedction
    public function MYSQLDB()
    {
        require_once __TOOLS__ . 'MySQLDatabase.php';
        return new MYSQLDB;
    }

    // validation
    public function Validate()
    {
        require_once __TOOLS__ . 'Validation.php';
        return new Validate;
    }

    // clients
    public function Client()
    {
        require_once __MODELS__ . 'Clients.php';
        return new Client;
    }

    // data grabbing
    public function Posted()
    {
        require_once __MODELS__ . 'Data.php';
        return new Posted;
    }

    // invoicing
    public function Invoicing()
    {
        require_once __MODELS__ . 'Invoices.php';
        return new Invoicing;
    }

    // projects
    public function Project()
    {
        require_once __MODELS__ . 'Projects.php';
        return new Project;
    }

    // tasks
    public function Task()
    {
        require_once __MODELS__ . 'Tasks.php';
        return new Task;
    }

    // users aka humans
    public function User()
    {
        require_once __MODELS__ . 'Users.php';
        return new User;
    }

    // Base function to handle Paths which will be used by each Gate
    public function path($path)
    {
        require_once __PATHS__ . $path . '.php';
    }
}
