<?php
/*
Default app document
 */

// Class start
class App
{

    // default gate
    protected $Gate = 'Home';
    // default method
    protected $method = 'index';

    // construct function to be ran when App is called
    public function __construct()
    {
        // place the uri inside a variable
        $uri = $this->get_uri();
        // convert [1] to lowercase then the first character to uppercase and place inside a variable
        $uri1 = ucfirst(strtolower($uri[1]));

        // check to see if the gate exists as a document
        if ( file_exists( __BASE__ . '/Gates/' . $uri1 . '.php' ) )
        {
            // if true, set this gate to the requested uri
            $this->Gate = $uri1;
        }
        // load the gate and instantiate the class
        require '../app/Gates/' . $this->Gate . '.php';
        $this->Gate = new $this->Gate;

        // check to see if the method exists inside the Gate
        if ( isset($uri[2]) )
        {
            // convert [2] to lowercase to avoid errors and place it in a variable
            $uri2 = strtolower($uri[2]);
            if ( method_exists($this->Gate, $uri2) )
            {
                // if the method exists, use it
                $this->method = $uri2;
            }
        }
        // call it! leave an empty array for a future update another day
        call_user_func_array([$this->Gate, $this->method], []);
    }

    // parse the url and sanitize it
    public function get_uri()
    {
        // check to see if the request uri is set
        if ( isset( $_SERVER['REQUEST_URI'] ) )
        {
            // if set, explode the string by a forward slash, sanitize the uri and return it
            return $uri = explode( '/', filter_var( $_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL ) );
        }
    }
}
