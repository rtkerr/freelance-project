<?php
/*
CONFIG document for easy linking and modifications of constants as configuration properties
 */

/*
============ Directory constants ============
 */

// BASE directory
define('__BASE__', __DIR__);
// Paths directory
define('__GATES__', __BASE__ . '/Gates/');
// Paths directory
define('__PATHS__', __BASE__ . '/Paths/');
// Partials directory
define('__PARTS__', __PATHS__ . '/Partials/');
// Models directory
define('__MODELS__', __BASE__ . '/Models/');
// Tools directory
define('__TOOLS__', __BASE__ . '/Tools/');

/*
============ Database constants ============
*/

// DB driver
define('__DBDRIVER__', 'mysql');
// DB name
define('__DBNAME__', 'somename');
// DB host
define('__DBHOST__', '127.0.0.1');
// DB port
define('__DBPORT__', '3306');
// DB Username
define('__DBUSER__', 'username');
// DB password
define('__DBPASS__', '********');
