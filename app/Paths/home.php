<?php
// require head document partial
require __PARTS__ . '/Head.php';
?>

<h1 class="title-lrg-char" id="">Freelance Project Management System</h1>
<h2 class="title-med-char" id="">OPEN SOURCED</h2>

<form class="form-std"id="" action="" method="post">

  <label class="label-char-fw" id="" for="username">Username:</label>
  <input class="input-light-fw" id="" type="text" name="username" placeholder="enter your username">

  <label class="label-char-fw" id="" for="password">Password:</label>
  <input class="input-light-fw" id="" type="password" name="password" placeholder="enter your password">

  <input class="submit-char-std" id="" type="submit" name="submit" value="Log in">

</form>

<p class="text-base-lh" id="">
  This is a testing environment. NO real data is to be entered as security can not be guaranteed.<br>
  -- Student Project --<br>
  2016
</p>

<footer class="foot-char" id="">
  <p class="" id="">
    There are no Privacy or Security Policies enforced on this system. Use at your own risk.
  </p>
</footer>

<?php
// require foot document partial
require __PARTS__ . '/Foot.php';
