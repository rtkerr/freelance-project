<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= $this->title?></title>
        <link rel="stylesheet" href="./assets/css/global.css" media="screen">
        <link href="https://fonts.googleapis.com/css?family=Prompt:200,300,500" rel="stylesheet">
        <script type="text/javascript" src="#"></script>
        <meta name="author" content="http://rtkerr.com">
    </head>
    <body>
<?php
// Place NAV on every page except the landing page
if( $this->title != 'Freelance Project' ) {
  // require navigation partial
  require __PARTS__ . '/Nav.php';
}
?>
        <main>
