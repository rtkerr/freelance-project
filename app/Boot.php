<?php
/*
BOOT document
everything is loaded here then passed off to index.php
 */

// sessions
session_start();

// error checking
ini_set('display_errors', 1);

// Config file for easy referencing
require __DIR__ . '/Config.php';

// Bring in the base app and controllers
require __BASE__ . '/Core/Controller.php';
require __BASE__ . '/Core/App.php';
