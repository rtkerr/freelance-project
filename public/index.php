<?php
/*
BASE render document pointing to boot.php for all rewrites to this
 */

// Load in the boot document
require '../app/Boot.php';

// Instantiate /Core/App
$app = new App();
