freelance-project
======

#### What is this?

This is currently a student project for a diploma. 

It is going to be, in time, a project management system for freelancers.
It will handle:

* Projects
  * Tasks
  * Invoices
* Clients
  * Private
  * Public

### System Specs ... the juicy stuff

| The name | What it is   |
| -------- | ------------ |
| O/S      | Ubuntu 16.04 |
| Server   | Nginx        |
| PHP      | PHP-FPM      |
| Database | MariaDB      |


##### Misc. Information

* This will not feature an advanced routing system, it will be kept simple
* There will be mess! in the code. Will be fixed along the way after commits and when the time is right

## Space magic workings

![space magic](project_images/routerphp.png)